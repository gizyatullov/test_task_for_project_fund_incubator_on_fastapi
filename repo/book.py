from fastapi import HTTPException, status
from sqlalchemy.orm import Session

import db
import models

__all__ = [
    'get_all_book_of_table',
    'create_book',
    'discard_book',
]


def get_all_book_of_table(session: Session, token: str) -> list[db.BaseBookMap]:
    try:
        table = db.tokens_tables[token]
    except KeyError:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail='Invalid token.'
        )
    else:
        return db.get_of_table(session=session, table=table)


def create_book(session: Session, request: models.BookInputModel):
    if db.check_exist_from_param(session=session,
                                 table=db.names_tables[request.category],
                                 id=request.id):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail='id invalid'
        )

    return db.create_entity_in_table(session=session,
                                     table=db.names_tables[request.category],
                                     data=request.dict(exclude={'category'}))


def discard_book(session: Session, request: models.BookDiscardModel):
    if not db.check_exist_from_param(session=session,
                                     table=db.names_tables[request.category],
                                     **request.dict(exclude={'category'})):
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND
        )

    db.del_obj_from_table(session=session,
                          table=db.names_tables[request.category],
                          **request.dict(exclude={'category'}))
