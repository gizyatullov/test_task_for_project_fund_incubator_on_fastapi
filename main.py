from fastapi import FastAPI
import uvicorn

import settings
import db
import routers

app = FastAPI(
    debug=True,
    title='test_task_for_project_fund_incubator_on_fastapi',
    version='0.0.1',
    license='MIT',
    contact={'telegram': '@gizyatullov0'}
)

app.include_router(router=routers.book_router)

if __name__ == '__main__':
    uvicorn.run(
        app='main:app',
        host='0.0.0.0',
        port=5194,
        reload=True
    )
