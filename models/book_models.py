from typing import Literal

from pydantic import BaseModel

import db

__all__ = [
    'BookModel',
    'BookInputModel',
    'BookOutputModel',
    'BookDiscardModel',
]


class BookModel(BaseModel):
    name: str
    author: str

    class Config:
        orm_mode = True


class BookOutputModel(BookModel):
    id: int


class BookInputModel(BookModel):
    id: int
    category: Literal['child', 'adult']


class BookDiscardModel(BookModel):
    category: Literal['child', 'adult']
