from sqlalchemy.orm import Session

__all__ = [
    'check_exist_from_param',
    'del_obj_from_table',
    'get_of_table',
    'create_entity_in_table',
]


def check_exist_from_param(session: Session, table, **kwargs) -> bool:
    if session.query(table).filter_by(**kwargs).scalar():
        return True
    return False


def del_obj_from_table(session: Session, table, **kwargs):
    session.query(table).filter_by(**kwargs).delete()
    session.commit()


def get_of_table(session: Session, table, **kwargs) -> list:
    return session.query(table).filter_by(**kwargs).all()


def create_entity_in_table(session: Session, table, data: dict):
    new_entity = table(**data)
    session.add(new_entity)
    session.commit()
    session.refresh(new_entity)
    return new_entity
