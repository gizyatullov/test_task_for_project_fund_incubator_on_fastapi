from os import getenv
from uuid import uuid4
from pprint import pprint

from sqlalchemy import create_engine
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import sessionmaker, Session
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy_utils import database_exists, create_database
from sqlalchemy import Column, Integer, String
from mixer.backend.sqlalchemy import Mixer

from .utils import *

URL = f"postgresql+psycopg2://{getenv('PG_USER')}:{getenv('PG_PASSWORD')}@{getenv('PG_IP')}:" \
      f"{getenv('PG_PORT')}/{getenv('PG_DB')}"

engine = create_engine(
    url=URL,
    echo=False
)

if not database_exists(url=engine.url):
    create_database(url=engine.url)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


def get_session():
    session = SessionLocal()
    try:
        yield session
    finally:
        session.close()


class BaseBookMap(Base):
    __abstract__ = True

    id = Column(Integer, primary_key=True)
    name = Column(String(100), index=True)
    author = Column(String(100), index=True)


class BookForChild(BaseBookMap):
    __tablename__ = 'child'


class BookForAdult(BaseBookMap):
    __tablename__ = 'adult'


class CategoryToken(Base):
    __tablename__ = 'category_token'

    id = Column(Integer, primary_key=True)
    name = Column(String(100), index=True)
    token = Column(UUID(as_uuid=True), index=True)


Base.metadata.create_all(bind=engine)


def fill_category_token():
    with SessionLocal() as db:
        for table in BaseBookMap.__subclasses__():
            new = CategoryToken(name=table.__tablename__, token=uuid4())
            db.add(new)
        db.commit()


def check_fill_category_token():
    with SessionLocal() as session:
        if not session.query(CategoryToken).count():
            fill_category_token()


check_fill_category_token()


def get_tokens_names_tables() -> tuple[dict, dict]:
    with SessionLocal() as session:
        query = session.query(CategoryToken)
        titles_tables = {item.__tablename__: item for item in BaseBookMap.__subclasses__()}
        secrets_tables = {str(item.token): titles_tables[item.name] for item in query.all()}
        return secrets_tables, titles_tables


tokens_tables, names_tables = get_tokens_names_tables()
pprint(tokens_tables)
pprint(names_tables)


def fill_table(session: Session, table: Base, quantity: int = 10):
    """
    Добавляет в таблицу фейковые данные.
    """
    mixer = Mixer(session=session, commit=True)
    mixer.cycle(count=quantity).blend(scheme=table,
                                      name=mixer.FAKE,
                                      author=mixer.FAKE)


def check_fill_tables(tables: [Base], less_than: int = 10):
    """
    Проверяет заполненность таблиц данными и при необходимости дополняет.
    """
    with SessionLocal() as session:
        for table in tables:
            if session.query(table).count() < less_than:
                fill_table(session=session, table=table)


check_fill_tables(tables=names_tables.values())
