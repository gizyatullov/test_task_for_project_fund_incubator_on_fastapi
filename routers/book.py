from fastapi import APIRouter, status, Depends
from sqlalchemy.orm import Session

import db
import models
import repo

__all__ = [
    'book_router',
]

book_router = APIRouter(
    prefix='/book',
    tags=['book']
)
router = book_router


@router.get('',
            response_model=list[models.BookOutputModel],
            status_code=status.HTTP_200_OK)
def get_books_of_table(token: str,
                       session: Session = Depends(dependency=db.get_session)):
    return repo.get_all_book_of_table(session=session, token=token)


@router.post('',
             response_model=models.BookOutputModel,
             status_code=status.HTTP_201_CREATED)
def post_book(request: models.BookInputModel,
              session: Session = Depends(dependency=db.get_session)):
    return repo.create_book(session=session, request=request)


@router.delete('',
               status_code=status.HTTP_204_NO_CONTENT)
def delete_book(request: models.BookDiscardModel,
                session: Session = Depends(dependency=db.get_session)):
    return repo.discard_book(session=session, request=request)
